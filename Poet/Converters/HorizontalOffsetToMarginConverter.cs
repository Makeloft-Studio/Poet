﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Poet.Converters
{
	class HorizontalOffsetToMarginConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var horizontalOffset = (double)value;
			return new Thickness(horizontalOffset, 0d, 0d, 0d);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
