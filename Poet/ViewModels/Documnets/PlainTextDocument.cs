﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows;
using Ace;
using Microsoft.Win32;
using Poet.Assist;
using Poet.Models;
using Poet.Views;

namespace Poet.ViewModels.Documnets
{
	[DataContract]
	public class PlainTextDocument : ADocument<string, string>
	{		
		[DataMember]
		public string TempFile
		{
			get => Get(() => TempFile, Guid.NewGuid() + ".tmp");
			set => Set(() => TempFile, value);
		}
		
		public EncodingModel EncodingModel
		{
			get => Get(() => EncodingModel);
			set => Set(() => EncodingModel, value);
		}

		[DataMember]
		public double FontSize
		{
			get => Get(() => FontSize, 14);
			set => Set(() => FontSize, value);
		}

		private static uint _counter;
		private string _originalModel;
		
		public override void Expose()
		{
			base.Expose();
			Title = Title ?? "New" + ++_counter;
			EncodingModel = EncodingModel ?? EncodingModel.DefaultModel;

			this[Context.Get("SetEncoding")].Executed += (sender, args) =>
				EncodingModel = new EncodingView {Owner = Application.Current.MainWindow}.To(out var view).ShowDialog().Is(true)
					? view.DialogResult.To<EncodingModel>()
					: EncodingModel;
			
			this[Context.Get("SetEncodingInfo")].CanExecute += (sender, args) =>
				args.CanExecute = args.Parameter.IsNot(EncodingModel);
			
			this[Context.Get("SetEncodingInfo")].Executed += (sender, args) =>
				EncodingModel = args.Parameter.To<EncodingModel>();

			this[Context.Get("Explorer")].CanExecute += (sender, args) => args.CanExecute = Key.Is();
			this[Context.Get("Explorer")].Executed += (sender, args) => Process.Start("explorer", $"/select, {Key}");

			this[Context.Get("Cmd")].CanExecute += (sender, args) => args.CanExecute = Key.Is();
			this[Context.Get("Cmd")].Executed += (sender, args) =>
				Process.Start("cmd", $"/K \"cd {System.IO.Path.GetDirectoryName(Key)}\"");
			
			this[() => Value].PropertyChanged += (sender, args) => HasChanged = Value != _originalModel;
			this[() => FontSize].ValidationRules += n =>
				4.0 < FontSize && FontSize < 128.0
					? null
					: LocalizationSource.Wrap["InvalidFontSize"];
		}

		public override async Task<bool> TryLoadAsync() => await Handle(Load);
		public override async Task<bool> TrySaveAsync() => Key.Is() ? await Handle(Save) : await Handle(SaveAs);
		public override async Task<bool> TrySaveAsAsync() => await Handle(SaveAs);
		public override async Task<bool> TryCloseAsync() => await Handle(Close);
		
		private async Task<string> GetKey() => (Key.IsNot() || HasChanged) && await Wrap.Storage.Exists(TempFile)
			? TempFile
			: Key;

		private async Task<bool> Load() => (Value = _originalModel = (await GetKey()).Is(out var key)
			? await Wrap.Storage.LoadText(key, EncodingModel.Encoding)
			: null).Put(key.Is());

		public async Task SaveBackup() => await SaveBy(TempFile);
		private async Task SaveOriginal() => await SaveBy(Key);

		private async Task SaveBy(string key)
		{
			await Wrap.Storage.SaveText(key, Value, EncodingModel.Encoding);
			_originalModel = Value;
		}

		private async Task<bool> Save()
		{
			await SaveOriginal();
			return true.With(HasChanged = false);
		}
		
		private async Task<bool> SaveAs()
		{	
			var result = ShowDialog(() =>
				new SaveFileDialog
				{
					Filter = "",
					FileName = Key
				}.To(out var dialog).ShowDialog().Is(true)
					? dialog.FileName
					: null);
			
			if (result.IsNot()) return false;
			Key = result;
			return await Save();			
		}

		public async Task DeleteBackupAsync() => await DeleteBy(TempFile);
		private async Task DeleteOriginal() => await DeleteBy(Key);
		private static async Task DeleteBy(string key) => await Wrap.Storage.Delete(key);
		
		private async Task<bool> Reload()
		{
			if (!HasChanged || !Key.Is()) return true;
			var result = ShowDialog(() => MessageBox.Show("Reload file?", Title, MessageBoxButton.YesNo));
			if (result == MessageBoxResult.Yes) return await TryLoadAsync();
			_originalModel = await Wrap.Storage.LoadText(Key, EncodingModel.Encoding);
			return true;
		}
		
		private async Task<bool> Close()
		{
			if (!HasChanged) return true;
			var result = ShowDialog(() => MessageBox.Show("Save changes?", Title, MessageBoxButton.YesNoCancel));					
			return result != MessageBoxResult.Cancel && (result.IsNot(MessageBoxResult.Yes) || await TrySaveAsync());
		}

		private T ShowDialog<T>(Func<T> dialog)
		{
			Store.Get<CoreViewModel>().ActiveDocument.To(out var tmp); 
			Context.Activate.GetMediator(this).Execute(null);

			dialog().To(out var result);
				
			Context.Activate.GetMediator(tmp, this).Execute(null);
			return result;
		}
	}
}