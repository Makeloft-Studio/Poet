using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows;
using Ace;
using Microsoft.Win32;
using Poet.Assist;
using Poet.Models;
using Poet.Views;

namespace Poet.ViewModels.Documnets
{
	public class Model : ContextObject
	{
		private MultiLineDocument _owner;

		public Model(string text, MultiLineDocument owner)
		{
			_owner = owner;
			Text = _initialText = text;
		}

		public string _initialText;
		private string _text;

		public int Number => _owner.Value.IndexOf(this) + 1;

		private static char[] _separators = { '\n', '\r' };
		public string Text
		{
			get => _text;
			set
			{
				var lines = value.Split(_separators, StringSplitOptions.RemoveEmptyEntries);
				if (lines.Length.Is(0)) return;
				_text = lines[0];
				var number = _owner.Value.Is() ? _owner.Value.IndexOf(this) : 0;
				for (var i = 1; i < lines.Length; i++)
				{
					_owner.Value.Insert(number + i, new Model(lines[i], _owner));
				}

				if (lines.Length > 1)
				{
					for (var i = number; i < _owner.Value.Count; i++)
						_owner.Value[i].EvokePropertyChanged(nameof(Number));
				}

				if (_owner.IsNot() || _owner.Value.IsNot()) return;
				_owner.HasChanged = HasChanges || _owner.Value.Any(m => m.HasChanges);
				EvokePropertyChanged(nameof(Text));
			}
		}

		public bool HasChanges => _initialText.IsNot(Text);
	}

	[DataContract]
	public class MultiLineDocument : ADocument<string, SmartSet<Model>>
	{		
		[DataMember]
		public string TempFile
		{
			get => Get(() => TempFile, Guid.NewGuid() + ".tmp");
			set => Set(() => TempFile, value);
		}
		
		public EncodingModel EncodingModel
		{
			get => Get(() => EncodingModel);
			set => Set(() => EncodingModel, value);
		}

		[DataMember]
		public double FontSize
		{
			get => Get(() => FontSize, 14);
			set => Set(() => FontSize, value);
		}

		private static uint _counter;
		private SmartSet<Model> _originalModel;
		
		public override void Expose()
		{
			base.Expose();
			Title = Title ?? "New" + ++_counter;
			EncodingModel = EncodingModel ?? EncodingModel.DefaultModel;

			this[Context.Get("SetEncoding")].Executed += (sender, args) =>
				EncodingModel = new EncodingView {Owner = Application.Current.MainWindow}.To(out var view).ShowDialog().Is(true)
					? view.DialogResult.To<EncodingModel>()
					: EncodingModel;
			
			this[Context.Get("SetEncodingInfo")].CanExecute += (sender, args) =>
				args.CanExecute = args.Parameter.IsNot(EncodingModel);
			
			this[Context.Get("SetEncodingInfo")].Executed += (sender, args) =>
				EncodingModel = args.Parameter.To<EncodingModel>();

			this[Context.Get("Explorer")].CanExecute += (sender, args) => args.CanExecute = Key.Is();
			this[Context.Get("Explorer")].Executed += (sender, args) => Process.Start("explorer", $"/select, {Key}");

			this[Context.Get("Cmd")].CanExecute += (sender, args) => args.CanExecute = Key.Is();
			this[Context.Get("Cmd")].Executed += (sender, args) =>
				Process.Start("cmd", $"/K \"cd {System.IO.Path.GetDirectoryName(Key)}\"");

			this[() => Value].PropertyChanged += (sender, args) => HasChanged = Value.Is() && Value.Any(m => m.HasChanges);
			this[() => FontSize].ValidationRules += n =>
				4.0 < FontSize && FontSize < 128.0
					? null
					: LocalizationSource.Wrap["InvalidFontSize"];
		}

		public override async Task<bool> TryLoadAsync() => await Handle(Load);
		public override async Task<bool> TrySaveAsync() => Key.Is() ? await Handle(Save) : await Handle(SaveAs);
		public override async Task<bool> TrySaveAsAsync() => await Handle(SaveAs);
		public override async Task<bool> TryCloseAsync() => await Handle(Close);
		
		private async Task<string> GetKey() => (Key.IsNot() || HasChanged) && await Wrap.Storage.Exists(TempFile)
			? TempFile
			: Key;

		private async Task<bool> Load() => (Value = _originalModel = (await GetKey()).Is(out var key)
			? (await Wrap.Storage.LoadLines(key, EncodingModel.Encoding)).Select(s => new Model(s, this)).ToSet()
			: null).Put(key.Is());

		public async Task SaveBackup() => await SaveBy(TempFile);
		private async Task SaveOriginal() => await SaveBy(Key);

		private async Task SaveBy(string key)
		{
			await Wrap.Storage.SaveLines(key, Value.Select(m => m.Text), EncodingModel.Encoding);
			_originalModel = Value;
		}

		private async Task<bool> Save()
		{
			await SaveOriginal();
			return true.With(HasChanged = false);
		}
		
		private async Task<bool> SaveAs()
		{	
			var result = ShowDialog(() =>
				new SaveFileDialog
				{
					Filter = "",
					FileName = Key
				}.To(out var dialog).ShowDialog().Is(true)
					? dialog.FileName
					: null);
			
			if (result.IsNot()) return false;
			Key = result;
			return await Save();			
		}

		public async Task DeleteBackupAsync() => await DeleteBy(TempFile);
		private async Task DeleteOriginal() => await DeleteBy(Key);
		private static async Task DeleteBy(string key) => await Wrap.Storage.Delete(key);
		
		private async Task<bool> Reload()
		{
			if (!HasChanged || !Key.Is()) return true;
			var result = ShowDialog(() => MessageBox.Show("Reload file?", Title, MessageBoxButton.YesNo));
			if (result == MessageBoxResult.Yes) return await TryLoadAsync();
			_originalModel = (await Wrap.Storage.LoadLines(Key, EncodingModel.Encoding)).Select(s => new Model(s, this)).ToSet();
			return true;
		}
		
		private async Task<bool> Close()
		{
			if (!HasChanged) return true;
			var result = ShowDialog(() => MessageBox.Show("Save changes?", Title, MessageBoxButton.YesNoCancel));					
			return result != MessageBoxResult.Cancel && (result.IsNot(MessageBoxResult.Yes) || await TrySaveAsync());
		}

		private T ShowDialog<T>(Func<T> dialog)
		{
			Store.Get<CoreViewModel>().ActiveDocument.To(out var tmp); 
			Context.Activate.GetMediator(this).Execute(null);

			dialog().To(out var result);
				
			Context.Activate.GetMediator(tmp, this).Execute(null);
			return result;
		}
	}
}