﻿using Ace;
using Poet.Models;

namespace Poet.ViewModels
{
	[DataContract]
	public class EncodingViewModel : SmartViewModel
	{
		public EncodingModel[] SystemEncodings { get; } = EncodingModel.EncodingModels;
	}
}