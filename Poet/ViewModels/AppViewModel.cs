﻿using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Ace;
using Poet.Languages;
using Poet.Models;
using Application = System.Windows.Application;
using FontFamily = System.Windows.Media.FontFamily;

namespace Poet.ViewModels
{	
	[DataContract]
	public class AppViewModel : SmartViewModel
	{
		[DataMember]
		public string Language
		{
			get => Get(() => Language);
			set => Set(() => Language, value);
		}

		public SmartSet<ResourceManager> Languages { get; } = new SmartSet<ResourceManager>
		{
			English.ResourceManager,
			Russian.ResourceManager,
			Belarussian.ResourceManager
		};

		public EncodingModel[] SystemEncodings { get; } = EncodingModel.EncodingModels;
		public EncodingModel[] Encodings { get; } = EncodingModel.MainEncodingModels;
		
		public EncodingModel DefaultEncodingModel { get; set; }

		public SmartSet<FontFamily> FontFamilies { get; set; } = new SmartSet<FontFamily>(Fonts.SystemFontFamilies);

		public NotifyIcon TrayView { get; } = new NotifyIcon
		{
			//Icon = new Icon(new MemoryStream((byte[])Properties.Resources.ResourceManager.GetObject("App"))), 
			Icon = Icon.ExtractAssociatedIcon(Assembly.GetEntryAssembly().Location),
			Text = LocalizationSource.Wrap["AppTitle"]
		};

		[DataMember]
		public bool ShowInTray
		{
			get => Get(() => ShowInTray);
			set => Set(() => ShowInTray, value);
		}

		[DataMember]
		public bool ShowInTaskBar
		{
			get => Get(() => ShowInTaskBar);
			set => Set(() => ShowInTaskBar, value);
		}

		public override void Expose()
		{
			base.Expose();

			this[() => ShowInTray].PropertyChanged += (sender, args) =>
				TrayView.Visible = ShowInTray;

			this[() => ShowInTaskBar].PropertyChanged += (sender, args) =>
				Application.Current.MainWindow.To(out var view)?.With(view.ShowInTaskbar = ShowInTaskBar);
			
			var isAppHidden = false;
			var windowState = WindowState.Normal;
			this[Context.Get("Hide")].Executed += (sender, args) =>
				Application.Current.MainWindow.To(out var view)?.With(
					isAppHidden = true,
					TrayView.Visible = true,
					view.ShowInTaskbar = false,
					windowState = view.WindowState,
					view.WindowState = WindowState.Minimized);
			
			this[Context.Get("Show")].Executed += (sender, args) =>
				Application.Current.MainWindow.To(out var view)?.With(
					isAppHidden = false,
					TrayView.Visible = ShowInTray,
					view.ShowInTaskbar = ShowInTaskBar,
					view.WindowState = windowState);
			
			var hideMediator = this.GetMediator("Hide", this);
			var showMediator = this.GetMediator("Show", this);
			TrayView.Click += (o, e) => (isAppHidden ? showMediator : hideMediator).Execute(null);
			TrayView.Visible = ShowInTray;
			
			this[Context.Get("SetLanguage")].CanExecute += (sender, args) =>
				args.CanExecute = args.Parameter.To<ResourceManager>().BaseName.IsNot(Language);

			this[Context.Get("SetLanguage")].Executed += (sender, args) =>
				Language = args.Parameter.To<ResourceManager>().BaseName;

			this[() => Language].PropertyChanged += (sender, args) =>
				LocalizationSource.Wrap.ActiveManager = Languages.FirstOrDefault(m => m.BaseName.Is(Language));

			Language = Language ?? English.ResourceManager.BaseName;
			
			//DefaultEncodingInfo = EncodingInfos.FirstOrDefault(i => i.CodePage == Encoding.Default.CodePage);
		}
	}
}