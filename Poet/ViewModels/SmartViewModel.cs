﻿using System.Diagnostics;
using System.Runtime.Serialization;
using Ace;

namespace Poet.ViewModels
{
	[DataContract]
	public class SmartViewModel : ContextObject, IExposable
	{
		public virtual void Expose()
		{
			this[Context.Get("SetSmartParameter")].CanExecute += (sender, args) =>
				args.CanExecute = Smart[args.Parameter.GetType().Name].IsNot(args.Parameter.ToString());

			this[Context.Get("SetSmartParameter")].Executed += (sender, args) =>
				Smart[args.Parameter.GetType().Name] = args.Parameter.ToString();
			
			this[Context.Get("SetCoreMenuAlignment")].CanExecute += (sender, args) =>
				args.CanExecute = Smart["CoreMenuAlignment"].IsNot(args.Parameter.ToString());

			this[Context.Get("SetCoreMenuAlignment")].Executed += (sender, args) =>
				Smart["CoreMenuAlignment"] = args.Parameter.ToString();

#if DEBUG
			SmartPropertyChanged += (sender, args) =>
				Debug.WriteLine($"{args.PropertyName}\t{this[args.PropertyName]}");
#endif
		}
	}
}