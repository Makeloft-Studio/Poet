﻿using System.Threading.Tasks;
using Ace;
using Poet.ViewModels.Documnets;

namespace Poet.Models
{
	[DataContract]
	public class FractalPresenter : ContextObject, IExposable
	{
		[DataMember]
		public ADocument Content { get; set; }

		[DataMember]
		public FractalPresenter ChildPresenter
		{
			get => Get(() => ChildPresenter);
			set => Set(() => ChildPresenter, value);
		}

		[DataMember]
		public bool IsExpanded
		{
			get => Get(() => IsExpanded);
			set => Set(() => IsExpanded, value);
		}

		public string SplitStateView
		{
			get => IsExpanded ? SplitState : "* Auto 0";
			set => SplitState = IsExpanded ? value : SplitState;
		}

		[DataMember] public string SplitState { get; set; } = "* Auto *";

		public FractalPresenter() => Expose();

		public void Expose()
		{
			this[Context.Get("Invert")].Executed += (sender, args) => IsExpanded = IsExpanded.Not();
			this[() => IsExpanded].PropertyChanged += (sender, args) => EvokePropertyChanged(() => SplitStateView);
			this[() => IsExpanded].PropertyChanged += (sender, args) =>
			{
				if (IsExpanded.Is(false)) return;
				ChildPresenter.OrNew().To(out var c).With
				(
					c.Content = Content,
					ChildPresenter = c
				);
			};
		}
	}
}