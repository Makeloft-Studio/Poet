﻿using System.Linq;
using System.Text;
using Ace;
using UTF16Encoding = System.Text.UnicodeEncoding;

namespace Poet.Models
{
	[DataContract]
	public class EncodingModel<TEncoding> : EncodingModel where TEncoding : Encoding
	{
		private readonly object[] _args;

		public EncodingModel(int codePage, object[] args = null) : base(codePage) => _args = args;

		protected override Encoding CreateEncoding() => Encoding.Default.CodePage.Is(CodePage) ? Encoding.Default :
			_args.IsNot()
			? SystemTable.First(i => i.CodePage.Is(CodePage)).GetEncoding()
			: New.Object<TEncoding>(_args);
	}

	[DataContract]
	public abstract class EncodingModel
	{
		protected static readonly EncodingInfo[] SystemTable = Encoding.GetEncodings();

		private static readonly object AllowOptionalCharactersOn = true;
		private static readonly object ByteOrderMarkPreambuleOff = false;
		private static readonly object ByteOrderMarkPreambuleOn = true;
		private static readonly object LittleEndian = false;
		private static readonly object BigEndian = true;

		public static readonly EncodingModel[] MainEncodingModels =
		{
			CreateModel<Encoding>(Encoding.Default.CodePage),
			CreateModel<ASCIIEncoding>(20127), /* us-ascii */
			CreateModel<UTF7Encoding>(65000, AllowOptionalCharactersOn), /* utf-7 */
			CreateModel<UTF8Encoding>(65001, ByteOrderMarkPreambuleOff), /* utf-8 */
			CreateModel<UTF8Encoding>(65001, ByteOrderMarkPreambuleOn), /* utf-8 • BOM */
			CreateModel<UTF16Encoding>(1201, LittleEndian, ByteOrderMarkPreambuleOff), /* utf-16LE */
			CreateModel<UTF16Encoding>(1200, LittleEndian, ByteOrderMarkPreambuleOn), /* utf-16LE • BOM */
			CreateModel<UTF16Encoding>(1201, BigEndian, ByteOrderMarkPreambuleOff), /* utf-16BE */
			CreateModel<UTF16Encoding>(1200, BigEndian, ByteOrderMarkPreambuleOn), /* utf-16BE • BOM */
			CreateModel<UTF32Encoding>(12000, LittleEndian, ByteOrderMarkPreambuleOff), /* utf-32LE */
			CreateModel<UTF32Encoding>(12000, LittleEndian, ByteOrderMarkPreambuleOn), /* utf-32LE • BOM */
			CreateModel<UTF32Encoding>(12001, BigEndian, ByteOrderMarkPreambuleOff), /* utf-32BE */
			CreateModel<UTF32Encoding>(12001, BigEndian, ByteOrderMarkPreambuleOn), /* utf-32BE • BOM */
		};

		private static readonly int[] MainCodepages = MainEncodingModels.Select(m => m.CodePage).ToArray();

		public static readonly EncodingModel[] EncodingModels =
			SystemTable.With(MainEncodingModels.Select(m => m.CodePage).ToArray()).Where(i => !MainCodepages.Contains(i.CodePage))
				.Select(i => new EncodingModel<Encoding>(i.CodePage)).Concat(MainEncodingModels)
				.OrderBy(e => e.Name).ToArray();

		public static readonly EncodingModel DefaultModel = MainEncodingModels[0];

		private static EncodingModel CreateModel<T>(int codePage, params object[] constructorArgs) where T : Encoding =>
			new EncodingModel<T>(codePage, constructorArgs);

		public EncodingModel(int codePage)
		{
			CodePage = codePage;
			var info = SystemTable.First(i => i.CodePage.Is(codePage));
			Name = info.Name;
			Description = info.DisplayName;
		}

		[DataMember]
		public int CodePage { get; set; }
		
		public string Name { get; }
		
		public string Description { get; }

		[DataMember]
		public bool TryUsePreamble { get; set; }
		
		private Encoding _encoding;

		protected abstract Encoding CreateEncoding();

		public Encoding Encoding => _encoding ?? CreateEncoding().To(out _encoding);
	}
}