﻿using System.Collections.Generic;

namespace Poet.Languages
{
	public class Belarussian : ALanguage<Belarussian>
	{
		public override Dictionary<string, string> GetDictionary() => new Dictionary<string, string>
		{
			{"File", "Файл"},
			{"Edit", "Праўка"},
			{"View", "Выгляд"},
			{"Help", "Дапамога"},
		};
	}
}