﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ace;
using Ace.Converters.Patterns;
using Poet.Models;
using Poet.ViewModels.Documnets;

namespace Poet.Dictionaries
{
	public partial class AppTemplates
	{
		public AppTemplates() => InitializeComponent();

		private void RecursiveTemplateConverter_OnConverting(object sender, ConverterEventArgs e) =>
			e.ConvertedValue = e.Value.Is() ? this[e.Parameter] : null;

		private void RootControl_DragEnter(object sender, System.Windows.DragEventArgs e)
		{
			e.Effects = System.Windows.DragDropEffects.Move;
		}

		private void RootControl_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
		{
			return;
			sender.To(out ListView listView);
			if (e.LeftButton.Is(System.Windows.Input.MouseButtonState.Pressed)) {
				e.OriginalSource.To<DependencyObject>().EnumerateVisualAncestors().OfType<ListViewItem>().First().To(out var listViewItem);

				var models = listView.DataContext.To<FractalPresenter>().Content.To<MultiLineDocument>().Value;
				var model = listViewItem.DataContext.To<Model>();
				DragDrop.DoDragDrop(listView, models.IndexOf(model).ToString(), DragDropEffects.Move);
			}
		}

		private void RootControl_Drop(object sender, DragEventArgs e)
		{
			return;
			sender.To(out ListView listView);
			int.TryParse((string)e.Data.GetData(DataFormats.StringFormat), out var index);
			var models = listView.DataContext.To<FractalPresenter>().Content.To<MultiLineDocument>().Value;
			var model = models[index];
			e.OriginalSource.To<DependencyObject>().EnumerateVisualAncestors().OfType<ListViewItem>().First().To(out var listViewItem);
			var targetModel = listViewItem.DataContext.To<Model>();
			var targetIndex = models.IndexOf(targetModel);
			models.Remove(model);
			models.Insert(targetIndex, model);
		}
	}
}
