﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ace;
using Args = System.Windows.DependencyPropertyChangedEventArgs;
using Dp = System.Windows.DependencyObject;
using static System.Windows.DependencyProperty;

namespace Poet.Behaviours
{
	public static class ScrollBehaviour
	{
		public static readonly DependencyProperty VerticalOffsetProperty = RegisterAttached(
			"VerticalOffset", typeof(double), typeof(ScrollBehaviour), new PropertyMetadata(default(double),
				(o, args) => o.Syncronize(GetSourceX, GetTargetX, SetSourceX, SetTargetX, true, GetTarget)));

		public static readonly DependencyProperty HorizontalOffsetProperty = RegisterAttached(
			"HorizontalOffset", typeof(double), typeof(ScrollBehaviour), new PropertyMetadata(default(double),
				(o, args) => o.Syncronize(GetSourceY, GetTargetY, SetSourceY, SetTargetY, true, GetTarget)));

		public static readonly DependencyProperty SyncModeProperty = RegisterAttached(
			"SyncMode", typeof(bool), typeof(ScrollBehaviour), new PropertyMetadata(default(bool),
				SyncChangedCallback));

		public static void SetHorizontalOffset(this Dp o, double value) => o.SetValue(HorizontalOffsetProperty, value);
		public static void SetVerticalOffset(this Dp o, double value) => o.SetValue(VerticalOffsetProperty, value);
		public static double GetHorizontalOffset(this Dp o) => (double)o.GetValue(HorizontalOffsetProperty);
		public static double GetVerticalOffset(this Dp o) => (double)o.GetValue(VerticalOffsetProperty);
		public static void SetSyncMode(this Dp o, bool value) => o.SetValue(SyncModeProperty, value);
		public static bool GetSyncMode(this Dp o) => (bool)o.GetValue(SyncModeProperty);

		private static void SyncChangedCallback(Dp sender, Args args)
		{
			sender.To(out FrameworkElement element);
			if (element.IsLoaded) sender.Setup();
			else element.Loaded += (s, eventArgs) => sender.Setup();
		}

		private static void Setup(this DependencyObject source)
		{
			var target = source.GetTarget();
			source.Syncronize(GetSourceX, GetTargetX, SetSourceX, SetTargetX, true, GetTarget, target);
			source.Syncronize(GetSourceY, GetTargetY, SetSourceY, SetTargetY, true, GetTarget, target);

			if (target.IsNot()) return;
			target.ScrollChanged += (o, e) =>
			{
				if (source.Is(out FrameworkElement el) && el.DataContext == null) return;
				source.Syncronize(GetSourceX, GetTargetX, SetSourceX, SetTargetX, false, GetTarget, target);
				source.Syncronize(GetSourceY, GetTargetY, SetSourceY, SetTargetY, false, GetTarget, target);
			};
		}

		private static ScrollViewer GetTarget(this DependencyObject element) =>
			element.EnumerateVisualDescendants().OfType<ScrollViewer>().FirstOrDefault();

		private static double GetSourceX(this DependencyObject source) => source.GetVerticalOffset();
		private static double GetSourceY(this DependencyObject source) => source.GetHorizontalOffset();
		private static double GetTargetX(this ScrollViewer target) => target.VerticalOffset;
		private static double GetTargetY(this ScrollViewer target) => target.HorizontalOffset;

		private static void SetSourceX(this DependencyObject source, double value) => source.SetVerticalOffset(value);
		private static void SetSourceY(this DependencyObject source, double value) => source.SetHorizontalOffset(value);
		private static void SetTargetX(this ScrollViewer target, double value) => target.ScrollToVerticalOffset(value);
		private static void SetTargetY(this ScrollViewer target, double value) => target.ScrollToHorizontalOffset(value);
	}
}