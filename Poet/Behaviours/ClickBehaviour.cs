﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Ace;

namespace Poet.Behaviours
{
	public static class ClickBehaviour
	{
		public static readonly DependencyProperty LowLatencyProperty =
			DependencyProperty.RegisterAttached("LowLatency", typeof(bool), typeof(ClickBehaviour),
				new PropertyMetadata(default(bool), PropertyChangedCallback));

		private static bool IsCheckable(this UIElement element) =>
			element is MenuItem menuItem && menuItem.IsCheckable || element is ToggleButton || element is Expander;

		private static void Invert(this UIElement element) => element.Match(
			(MenuItem menuItem) => menuItem.IsChecked = !menuItem.IsChecked,
			(ToggleButton toggleButton) => toggleButton.IsChecked = !toggleButton.IsChecked,
			(Expander expander) => expander.IsExpanded = !expander.IsExpanded,
			(UIElement _) => default,
			() => default);

		private static void PropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs args)
		{
			o.To(out UIElement element);
			o.As(out ICommandSource commandSource);

			if (args.NewValue.Is(false))
				element.PreviewMouseLeftButtonDown -= OnElementOnPreviewMouseLeftButtonDown;
			else if (args.NewValue.Is(true))
				element.PreviewMouseLeftButtonDown += OnElementOnPreviewMouseLeftButtonDown;

			void OnElementOnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
			{
				if (e.LeftButton != MouseButtonState.Pressed) return;
				var parameter = commandSource?.CommandParameter;
				var command = commandSource?.Command;
				if (command == null && !element.IsCheckable()) return;
				if (command != null && command.CanExecute(parameter))
				{
					//element.IsEnabled = false;
					command.Execute(parameter);
				}
				else if (element.IsCheckable())
					element.Invert();

				void DisposableHandler(object sender1, MouseButtonEventArgs eventArgs)
				{
					eventArgs.Handled = true;
					sender1.To<UIElement>().PreviewMouseLeftButtonUp -= DisposableHandler;
					sender1.To<UIElement>().ReleaseMouseCapture();
					//element.IsEnabled = command.CanExecute(parameter);
				}

				element.PreviewMouseLeftButtonUp += DisposableHandler;

				var host =
					element.EnumerateVisualAncestors().OfType<ItemsControl>().FirstOrDefault().To<FrameworkElement>() ??
					element.EnumerateVisualAncestors().OfType<ItemsPresenter>().FirstOrDefault().To<FrameworkElement>();
				if (host == null) return;
				host.PreviewMouseLeftButtonUp += DisposableHandler;
				//element.MouseLeave += DisposableHandler;

				e.Handled = true;
			}
		}

		public static bool GetLowLatency(UIElement element) => (bool) element.GetValue(LowLatencyProperty);
		public static void SetLowLatency(UIElement element, bool value) => element.SetValue(LowLatencyProperty, value);
	}
}