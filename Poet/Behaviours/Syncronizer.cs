﻿using System;
using Ace;
using Poet.ViewModels;

namespace Poet.Behaviours
{
    public static class Syncronizer
    {
        private static CoreViewModel _core = Store.Get<CoreViewModel>();
        
        public static void Syncronize<TSource, TTarget, TValue>(this TSource source,
            Func<TSource, TValue> getSourceValue,
            Func<TTarget, TValue> getTargetValue,
            Action<TSource, TValue> setSourceValue,
            Action<TTarget, TValue> setTargetValue,
            bool direct, Func<TSource, TTarget> getTarget, TTarget target = null)
            where TSource : class where TTarget : class 
        {
            target = target ?? getTarget(source);
            if (target.IsNot() || source.IsNot()) return;

            var sourceValueX = getSourceValue(source);
            var targetValueX = getTargetValue(target);

            if (sourceValueX.Is(targetValueX)) return;
            if (direct)
                setTargetValue(target, sourceValueX);
            else
                setSourceValue(source, targetValueX);
        }
    }
}