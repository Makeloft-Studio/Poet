﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Ace;
using static System.Windows.DependencyProperty;

namespace Poet.Behaviours
{
    public static class SelectionBehaviour
    {
        public static readonly DependencyProperty Start = RegisterAttached(
            "Start", typeof(int), typeof(SelectionBehaviour), new PropertyMetadata(default(int),
                (o, args) => o.To<TextBox>().Syncronize(GetSourceX, GetTargetX, SetSourceX, SetTargetX, true, GetTarget)));

        public static readonly DependencyProperty Length = RegisterAttached(
            "Length", typeof(int), typeof(SelectionBehaviour), new PropertyMetadata(default(int),
                (o, args) => o.To<TextBox>().Syncronize(GetSourceY, GetTargetY, SetSourceY, SetTargetY, true, GetTarget)));

        public static readonly DependencyProperty SyncModeProperty = RegisterAttached(
            "SyncMode", typeof(bool), typeof(SelectionBehaviour), new PropertyMetadata(default(bool),
                SyncChangedCallback));

        public static void SetLength(this TextBox element, int value) => element.SetValue(Length, value);
        public static void SetStart(this TextBox element, int value) => element.SetValue(Start, value);
        public static int GetLength(this TextBox element) => (int) element.GetValue(Length);
        public static int GetStart(this TextBox element) => (int) element.GetValue(Start);
        public static void SetSyncMode(this TextBox o, bool value) => o.SetValue(SyncModeProperty, value);
        public static bool GetSyncMode(TextBox o) => (bool) o.GetValue(SyncModeProperty);

        private static void SyncChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            sender.To(out TextBox textBox);
            if (textBox.IsLoaded) textBox.Setup();
            else textBox.Loaded += (o, e) => textBox.Setup();
        }

        private static void Setup(this TextBox source)
        {
            var target = source.GetTarget();
            source.Syncronize(GetSourceX, GetTargetX, SetSourceX, SetTargetX, true, GetTarget, target);
            source.Syncronize(GetSourceY, GetTargetY, SetSourceY, SetTargetY, true, GetTarget, target);
            
            Keyboard.Focus(source);
            
            var lastDataContextX = Const.Null;
            var lastDataContextY = Const.Null;
            
            source.SelectionChanged += (o, e) =>
            {
                if (source.DataContext.IsNot(lastDataContextX))
                {
                    lastDataContextX = source.DataContext;
                    return;
                }
                
                source.Syncronize(GetSourceX, GetTargetX, SetSourceX, SetTargetX, false, GetTarget, target);
                
                if (source.DataContext.IsNot(lastDataContextY))
                {
                    lastDataContextY = source.DataContext;
                    return;
                }
                
                source.Syncronize(GetSourceY, GetTargetY, SetSourceY, SetTargetY, false, GetTarget, target);
            };
        }

        private static TextBox GetTarget(this TextBox element) => element;
        
        private static int GetSourceX(this TextBox source) => source.GetStart();
        private static int GetSourceY(this TextBox source) => source.GetLength();
        private static int GetTargetX(this TextBox target) => target.SelectionStart;
        private static int GetTargetY(this TextBox target) => target.SelectionLength;

        private static void SetSourceX(this TextBox source, int value) => source.SetStart(value);
        private static void SetSourceY(this TextBox source, int value) => source.SetLength(value);
        private static void SetTargetX(this TextBox target, int value) => target.SelectionStart = value;
        private static void SetTargetY(this TextBox target, int value) => target.SelectionLength = value;
    }
}