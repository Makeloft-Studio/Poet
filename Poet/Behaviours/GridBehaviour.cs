﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;

namespace Poet.Behaviours
{
	public static class GridBehaviour
	{
		public static void SetAllowMultisorting(DataGrid element, bool value)
		{
			if (value) element.Sorting += dataGridName_Sorting;
			else element.Sorting -= dataGridName_Sorting;
		}

		private static void dataGridName_Sorting(object sender, DataGridSortingEventArgs e)
		{
			e.Handled = true;
			var dataGrid = (DataGrid) sender;
			var path = e.Column.SortMemberPath;
			var view = CollectionViewSource.GetDefaultView(dataGrid.ItemsSource);
			if (view == null) return;
			var sortDescription = view.SortDescriptions.FirstOrDefault(d => d.PropertyName == path);

			if (sortDescription == default)
			{
				view.SortDescriptions.Add(new SortDescription(path, ListSortDirection.Ascending));
			}
			else
			{
				view.SortDescriptions.Remove(sortDescription);
				if (sortDescription.Direction == ListSortDirection.Ascending)
					view.SortDescriptions.Add(new SortDescription(path, ListSortDirection.Descending));
			}
		}
	}
}
