﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using Ace;
using Poet.ViewModels.Documnets;

namespace Poet.Behaviours
{
	public static class WebBrowserExtensions
	{
		public static readonly DependencyProperty ViewModelProperty =
			DependencyProperty.RegisterAttached("ViewModel", typeof(object), typeof(WebBrowserExtensions),
		new PropertyMetadata(default, (o, e) =>
		{
			try
			{
				if (o.Is(out WebBrowser browser).Not()) return;
				var viewModel = browser.DataContext.To<WebDocument>();
				viewModel.Bind(browser);
			}
			catch (Exception exception)
			{
				Trace.WriteLine(exception);
			}
		}));

		public static void SetViewModel(this DependencyObject o, object value) => o.SetValue(ViewModelProperty, value);
		public static object GetViewModel(this DependencyObject o) => o.GetValue(ViewModelProperty);

		public static SHDocVw.WebBrowser GetNativeService(this WebBrowser webBrowser)
		{
			var serviceProvider = (IServiceProvider)webBrowser.Document;
			if (serviceProvider.IsNot()) return default;

			var serviceGuid = new Guid("0002DF05-0000-0000-C000-000000000046");
			var iid = typeof(SHDocVw.WebBrowser).GUID;
			var webBrowserPtr = (SHDocVw.WebBrowser)serviceProvider.QueryService(ref serviceGuid, ref iid);
			return webBrowserPtr;
		}

		[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		[Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
		internal interface IServiceProvider
		{
			[return: MarshalAs(UnmanagedType.IUnknown)]
			object QueryService(ref Guid guidService, ref Guid riid);
		}
	}
}
