﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ace;
using Microsoft.Win32;

namespace Poet.Assist
{
	class BrowserEmulation
	{
		const string IsBrowserEmulationEnabled = @"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION";
		const string IsBrowserEmulationEnabledX64 = @"SOFTWARE\WOW6432Node\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION";

		public static void EnableFor(string path)
		{

			try
			{
				using (var key = Registry.CurrentUser.OpenSubKey(IsBrowserEmulationEnabled, true))
				{
					var value = key.GetValue(path);
					if (value.IsNot(11000))
						key.SetValue(path, 11000, RegistryValueKind.DWord);

				}

			}
			catch (Exception exception)
			{

			}
		}
	}
}
