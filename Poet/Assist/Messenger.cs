﻿using System;
using System.ComponentModel;
using System.Windows;

namespace Poet.Assist
{
	internal class Messenger : Wrap.IMessanger
	{
		public bool? Ask(string question, string title = "") =>
			MessageBox.Show(question, title, MessageBoxButton.YesNo) == MessageBoxResult.Yes;

		public void Alert(string notification, string title = "") =>
			MessageBox.Show(notification, string.Empty, MessageBoxButton.OK);

		public void ConfirmExit(string question, string title = "", CancelEventArgs args = null)
		{
			if (Ask(question, title) == true) Environment.Exit(0);
			if (args != null) args.Cancel = true;
		}
	}
}